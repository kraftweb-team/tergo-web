# Generated by Django 3.0.14 on 2021-07-27 18:03

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('payment', '0005_payment_currency'),
    ]

    operations = [
        migrations.AddField(
            model_name='payment',
            name='gateway',
            field=models.CharField(default='', max_length=50),
        ),
        migrations.AlterField(
            model_name='payment',
            name='status',
            field=models.CharField(default='PENDING', max_length=50),
        ),
    ]
