from django.db import models

from django.http import HttpResponse, JsonResponse
from django.shortcuts import render, redirect

from django.utils.encoding import force_bytes
from django.utils.http import urlsafe_base64_encode

from django.contrib.auth import authenticate, login
from django.contrib.auth.tokens import default_token_generator
from django.contrib.sites.shortcuts import get_current_site

from django.template.loader import render_to_string

from account.forms import UserRegistrationForm, LoginForm
from .forms import PaymentForm
from .models import Payment, RecurringPayment

import random
import hashlib
import base64
import xmltodict
from django.utils.timezone import now
from datetime import timedelta, datetime, date

from django.db import connections
from django.views.decorators.csrf import csrf_exempt

from django_cron import CronJobBase, Schedule

def user_login(request):
  if request.method == 'POST':
    form = LoginForm(request.POST)

    if form.is_valid():
      cd = form.cleaned_data
      user = authenticate(username=cd['username'],
                          password=cd['password'])
      if user is not None:
        if user.is_active:
          login(request, user)
          
          return redirect('/offset/payment/')
          # return render(request, 'payment/payment.html', {'form': form})
        else:
          return HttpResponse('Konto jest zablokowane.')
      else:
        return HttpResponse('Nieprawidłowe dane uwierzytelniające.')
  else:
    form = LoginForm()
  return render(request, 'payment/login.html', {'form': form})

def register(request):
  if request.method == 'POST':
    user_form = UserRegistrationForm(request.POST)
    if user_form.is_valid():
      new_user = user_form.save(commit=False)
      new_user.is_active = False
      new_user.set_password(user_form.cleaned_data['password']) # Zapisanie obiektu User.
      new_user.save()

      current_site = get_current_site(request)
      mail_subject = 'Activate your account.'
      message = render_to_string('account/acc_active_email.html', {
          'user': new_user,
          'domain': current_site.domain,
          'uid': urlsafe_base64_encode(force_bytes(new_user.pk)),
          'token': default_token_generator.make_token(new_user),
      })

      to_email = user_form.cleaned_data.get('email')

      email = EmailMessage(
        mail_subject, message, to=[to_email]
      )

      email.send()

      return render(request, 'account/register_email.html', {'user_form': user_form}) 
  else:
    user_form = UserRegistrationForm()
  
  return render(request, 'payment/register.html', {'user_form': user_form})

def offset(request):
  if request.method == 'GET':
    return render(request, 'payment/offset.html')
  else:
    payment_form = PaymentForm(request.POST)

    if payment_form.is_valid():
      cd = payment_form.cleaned_data

      amount = format(float(cd['amount']), '.2f')
      currency = cd['currency']
      #description = cd['description']
      frequency = cd['frequency']

      request.session['amount'] = amount
      request.session['currency'] = currency
      #request.session['description'] = description
      request.session['frequency'] = frequency

      if request.user.is_authenticated:
        return redirect('/offset/payment')
      else:
        return redirect('/offset/login')
 
def payment(request):
  serviceKey = 'b5f978816f76db7fb9b256ca3a8615ab887cba6d'
  shopID = 903566

  if request.method == 'POST':
    payment_form = PaymentForm(request.POST)

    if payment_form.is_valid():
      cd = payment_form.cleaned_data

      amount = format(float(cd['amount']), '.2f')
      currency = cd['currency']
      frequency = cd['frequency']
      # description = cd['description']
      gatewayID = 1503
      orderID = random.randint(1000000, 100000000)
      recurringAction = 'INIT_WITH_PAYMENT'
      
      base = f"{shopID}|{orderID}|{amount}|{currency}|{serviceKey}"
      #base = f"{shopID}|{orderID}|{amount}|{gatewayID}|{recurringAction}|{serviceKey}"
      hashCode = hashlib.sha256(base.encode()).hexdigest()

      request.session['payment_hash'] = hashCode
      request.session['orderID'] = orderID

      RecurringPayment.objects.create(
        user = request.user.id,
        order = orderID,
        frequency = frequency,
      )

      Payment.objects.create(
        order = orderID,
        user = request.user.id,
        gateway = gatewayID,
        amount = amount,
        currency = currency,
        status = 'PENDING',
        isRecurring = True,
      )

      #response = HttpResponse(status = 301, headers={'BmHeader': 'pay-bm-continue-transaction-ur'})
      #response['Location'] = f"https://pay-accept.bm.pl/payment?ServiceID={shopID}&OrderID={orderID}&Amount={amount}&GatewayID=1503&RecurringAction={recurringAction}&Hash={hashCode}"

      response = HttpResponse(status = 301)
      response['Location'] = f"https://pay-accept.bm.pl/payment?ServiceID={shopID}&OrderID={orderID}&Amount={amount}&Currency={currency}&Hash={hashCode}"

      return response
  else:
    serviceID = request.GET.get('ServiceID')
    orderID = request.GET.get('OrderID')
    hashCode = request.GET.get('Hash')
    #sessionPaymentHash = request.session.get('payment_hash', False)

    amount = request.session.get('amount', False)
    currency = request.session.get('currency', False)
    frequency = request.session.get('frequency', False)

    payment_form = PaymentForm()

    return render(request, 'payment/payment.html', {
      'payment_form': payment_form,
      'amount': amount,
      'currency': currency,
      'frequency': frequency,
    })

def payment_done(request):
  serviceKey = 'b5f978816f76db7fb9b256ca3a8615ab887cba6d'

  if request.method == 'GET':
    if request.is_ajax():
      orderID = request.session.get('orderID', False)

      if orderID:
        try:
          payment = Payment.objects.get(order = orderID)
        except Payment.DoesNotExist:
          payment = None

        if payment is not None:
          return JsonResponse({'status': payment.status})
        else:
          return JsonResponse({'status': 'FAILURE'})
      else:
        return JsonResponse({'status': 'FAILURE'})
    else:
      serviceID = request.GET.get('ServiceID', None)
      orderID = request.GET.get('OrderID', None)
      hashCode = request.GET.get('Hash', None)

      if serviceID is not None\
        and orderID is not None\
        and hashCode is not None:

        testBase = f"{serviceID}|{orderID}|{serviceKey}"
        testHashCode = hashlib.sha256(testBase.encode()).hexdigest()

        if hashCode == testHashCode:
          try:
            payment = Payment.objects.get(order = orderID)
          except Payment.DoesNotExist:
            payment = None
          
          if payment is not None:
            return render(request, 'payment/payment_done.html', {'status': payment.status})
          else:
            return render(request, 'payment/payment_done.html', {'status': 'FAILURE'})
        else:
          return render(request, 'payment/payment_done.html', {'status': 'FAILURE'})
      else:
        return redirect('/offset/')

@csrf_exempt
def payment_status(request):
  serviceKey = 'b5f978816f76db7fb9b256ca3a8615ab887cba6d'

  if request.method == 'POST':
    transactions = request.POST.get('transactions', None)
    recurring = request.POST.get('recurring', None)

    if transactions is not None:
      decodedBytes = base64.b64decode(transactions)
      decodedStr = str(decodedBytes, "utf-8")

      doc = xmltodict.parse(decodedStr)

      serviceID = doc['transactionList']['serviceID']

      transaction = doc['transactionList']['transactions']['transaction']

      orderID = transaction['orderID']
      #remoteID = transaction['remoteID']
      #amount = transaction['amount']
      #currency = transaction['currency']
      gatewayID = transaction['gatewayID']
      #paymentDate = transaction['paymentDate']
      paymentStatus = transaction['paymentStatus']
      #paymentStatusDetails = transaction['paymentStatusDetails']

      try:
        payment = Payment.objects.get(order = orderID)
        payment.status = paymentStatus
        payment.save(update_fields=['status'])

      except Payment.DoesNotExist:
        payment = None

      if payment is not None:
        confirmation = 'CONFIRMED'
      else:
        confirmation = 'NOTCONFIRMED'

      baseCode = f"{serviceID}|{orderID}|{confirmation}|{serviceKey}"
      hashCode = hashlib.sha256(baseCode.encode()).hexdigest()

      responseContent = f"<?xml version=\"1.0\" encoding=\"UTF-8\"?>\
      <confirmationList>\
        <serviceID>{serviceID}</serviceID>\
        <transactionsConfirmations>\
          <transactionConfirmed>\
            <orderID>{orderID}</orderID>\
            <confirmation>{confirmation}</confirmation>\
          </transactionConfirmed>\
        </transactionsConfirmations>\
        <hash>{hashCode}</hash>\
      </confirmationList>"

      return HttpResponse(responseContent)
        
    elif recurring is not None:
      decodedBytes = base64.b64decode(recurring)
      decodedStr = str(decodedBytes, "utf-8")

      doc = xmltodict.parse(decodedStr)

      serviceID = doc['recurringActivation']['serviceID']
      transaction = doc['recurringActivation']['transaction']
      #recurringData = doc['recurringActivation']['recurringData']

      orderID = transaction['orderID']
      #remoteID = transaction['remoteID']
      #amount = transaction['amount']
      #currency = transaction['currency']
      gatewayID = transaction['gatewayID']
      #paymentDate = transaction['paymentDate']
      paymentStatus = transaction['paymentStatus']
      #paymentStatusDetails = transaction['paymentStatusDetails']
      #recurringAction = recurringData['recurringAction']
      clientHash = transaction['clientHash']

      payment = Payment.objects.filter(order = orderID).last()

      if payment:
        payment.status = paymentStatus
        payment.save(update_fields=['status'])

      try:
        recurringPayment = RecurringPayment.objects.get(order = orderID)

        recurringPayment.clientHash = clientHash

        if paymentStatus == 'SUCCESS':
          recurringPayment.isActive = True
          recurringPayment.lastPayment = now

        recurringPayment.save(update_fields=['clientHash', 'isActive', 'lastPayment'])

      except RecurringPayment.DoesNotExist:
        recurringPayment = None

      if payment is not None\
        and recurringPayment is not None:

        confirmation = 'CONFIRMED'
      else:
        confirmation = 'NOTCONFIRMED'

      baseCode = f"{serviceID}|{clientHash}|{confirmation}|{serviceKey}"
      hashCode = hashlib.sha256(baseCode.encode()).hexdigest()

      responseContent = f"<?xml version=\"1.0\" encoding=\"UTF-8\"?>\
      <confirmationList>\
        <serviceID>{serviceID}</serviceID>\
        <recurringConfirmations>\
          <recurringConfirmed>\
            <clientHash>{clientHash}</clientHash>\
            <confirmation>{confirmation}</confirmation>\
          </recurringConfirmed>\
        </recurringConfirmations>\
        <hash>{hashCode}</hash>\
      </confirmationList>"

      return HttpResponse(responseContent)
    return HttpResponse()
  return HttpResponse()


class MyCronJob(CronJobBase):
  RUN_EVERY_MINS = 120 # every 2 hours

  schedule = Schedule(run_every_mins=RUN_EVERY_MINS)
  code = 'my_app.my_cron_job'

  def do(self):
    currentDate = datetime.today()

    recurringPayments = RecurringPayment.objects.filter(isActive = True, nextPayment = currentDate)

    for payment in recurringPayments:
      pass;
    