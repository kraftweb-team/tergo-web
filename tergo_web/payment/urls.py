from django.urls import path
from . import views as payment_views

urlpatterns = [
  path('', payment_views.offset, name='offset'),
  path('login/', payment_views.user_login, name="offset-login"),
  path('register/', payment_views.register, name="offset-register"),

  path('payment/', payment_views.payment, name='payment'),
  path('payment_done/', payment_views.payment_done, name='payment_done'),
  path('payment_status/', payment_views.payment_status, name='payment_status'),
  path('payment_status', payment_views.payment_status, name='payment_status'),
]