from django import forms

class PaymentForm(forms.Form):
  amount = forms.CharField(label='Amount')
  currency = forms.CharField(label='Currency')
  frequency = forms.CharField(label='Frequency')
  #description = forms.CharField(label='Description')