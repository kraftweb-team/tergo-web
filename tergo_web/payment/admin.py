from django.contrib import admin

from .models import Payment, RecurringPayment

@admin.register(Payment)
class PaymentAdmin(admin.ModelAdmin):
  list_display = ['order', 'paymentDate', 'user', 'amount', 'currency', 'status']

@admin.register(RecurringPayment)
class RecurringPaymentAdmin(admin.ModelAdmin):
  list_display = ['order', 'user', 'activeDate', 'frequency', 'isActive', 'lastPayment']