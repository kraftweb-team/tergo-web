from django.db import models

class Payment(models.Model):
  order = models.CharField(max_length=50)
  paymentDate = models.DateTimeField(auto_now=True)
  user = models.CharField(max_length=50)
  gateway = models.CharField(max_length=10)
  amount = models.CharField(max_length=50)
  currency = models.CharField(max_length=50)
  description = models.CharField(max_length=250, default='')
  status = models.CharField(max_length=10, default='PENDING')
  isRecurring = models.BooleanField(default=False)
  
class RecurringPayment(models.Model):
  order = models.CharField(max_length=50)
  user = models.CharField(max_length=50)
  clientHash = models.CharField(max_length=250)
  activeDate = models.DateTimeField(auto_now_add=True)
  expirationDate = models.CharField(max_length=25, default='')
  nextPayment = models.DateField(null=True)
  frequency = models.CharField(max_length=25)
  isActive = models.BooleanField(default=False)
  lastPayment = models.DateTimeField(null=True)