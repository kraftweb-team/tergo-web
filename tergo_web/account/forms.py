from django import forms
from django.contrib.auth import get_user_model

from django.contrib.auth.forms import ReadOnlyPasswordHashField

User = get_user_model()

class LoginForm(forms.Form):
  username = forms.CharField(label='E-mail')
  password = forms.CharField(widget=forms.PasswordInput)

class UserRegistrationForm(forms.ModelForm):
  email = forms.EmailField(label='E-mail')
  first_name = forms.CharField(label='Imię', required = False)
  last_name = forms.CharField(label='Nazwisko', required = False)
  password = forms.CharField(label='Hasło',
                            widget=forms.PasswordInput)
  password2 = forms.CharField(label='Powtórz hasło',
                            widget=forms.PasswordInput)

  class Meta:
    model = User
    fields = ('email', 'first_name', 'last_name')

  def clean_password2(self):
    cd = self.cleaned_data
    if cd['password'] != cd['password2']:
      raise forms.ValidationError('Hasła nie są identyczne.')
    return cd['password2']

class UserAdminCreationForm(forms.ModelForm):
    """
    A form for creating new users. Includes all the required
    fields, plus a repeated password.
    """
    password = forms.CharField(widget=forms.PasswordInput)
    password_2 = forms.CharField(label='Confirm Password', widget=forms.PasswordInput)

    class Meta:
        model = User
        fields = ['email']

    def clean(self):
        '''
        Verify both passwords match.
        '''
        cleaned_data = super().clean()
        password = cleaned_data.get("password")
        password_2 = cleaned_data.get("password_2")
        if password is not None and password != password_2:
            self.add_error("password_2", "Your passwords must match")
        return cleaned_data

    def save(self, commit=True):
        # Save the provided password in hashed format
        user = super().save(commit=False)
        user.set_password(self.cleaned_data["password"])
        if commit:
            user.save()
        return user


class UserAdminChangeForm(forms.ModelForm):
    """A form for updating users. Includes all the fields on
    the user, but replaces the password field with admin's
    password hash display field.
    """
    password = ReadOnlyPasswordHashField()

    class Meta:
        model = User
        fields = ['email', 'password', 'is_active', 'admin']

    def clean_password(self):
        # Regardless of what the user provides, return the initial value.
        # This is done here, rather than on the field, because the
        # field does not have access to the initial value
        return self.initial["password"]

class UserEditForm(forms.ModelForm):
  password = forms.CharField(widget=forms.PasswordInput)
  class Meta:
    model = User
    fields = ('first_name', 'last_name', 'email', 'password')

class SubscriptionForm(forms.ModelForm):
  email = forms.EmailField(label='E-mail')

class ContactForm(forms.ModelForm):
  email = forms.EmailField(label='E-mail')
  last_name = forms.CharField(label='Nazwisko')


