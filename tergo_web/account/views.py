from django.http import HttpResponse, JsonResponse
from django.shortcuts import render
from django.contrib.auth import authenticate, login
from django.contrib.auth.decorators import login_required
from django.contrib import messages
from .forms import LoginForm, UserRegistrationForm, UserEditForm, SubscriptionForm, ContactForm
from django.views.decorators.csrf import csrf_exempt

from django.contrib.auth.tokens import default_token_generator
from django.contrib.sites.shortcuts import get_current_site
from django.core.mail import EmailMessage
from django.http import HttpResponse
from django.template.loader import render_to_string
from django.utils.encoding import force_bytes
from django.utils.http import urlsafe_base64_encode, urlsafe_base64_decode

from django.contrib.auth import get_user_model
from django.contrib.auth.hashers import make_password

from payment.models import Payment

import random
import hashlib

from django.db import connections

UserModel = get_user_model()
User = get_user_model()

def user_login(request):
  if request.method == 'POST':
    form = LoginForm(request.POST)
    if form.is_valid():
      cd = form.cleaned_data
      user = authenticate(username=cd['username'],
                          password=cd['password'])
      if user is not None:
        if user.is_active:
          login(request, user)
          return HttpResponse('Uwierzytelnienie zakończyło się sukcesem.')
        else:
          return HttpResponse('Konto jest zablokowane.')
      else:
        return HttpResponse('Nieprawidłowe dane uwierzytelniające.')
  else:
    form = LoginForm()
  return render(request, 'account/login.html', {'form': form})

@login_required
def dashboard(request):
  if request.method == 'POST':
    user_form = UserEditForm(instance=request.user,
                              data=request.POST)
    if user_form.is_valid():
      user = user_form.save(commit=False)
      user.password = make_password(user.password)
      user.save()

      messages.success(request, 'Profile updated successfully')
    else:
      messages.error(request, 'Error updating your profile')
  else:
    user_form = UserEditForm(instance=request.user)

  return render(request,
                'account/dashboard.html',
                {'section': 'dashboard', 'user_form': user_form, 'payments': Payment.objects.filter(user=request.user.id)})

def register(request):
  if request.method == 'POST':
    user_form = UserRegistrationForm(request.POST)
    if user_form.is_valid():
      new_user = user_form.save(commit=False)
      new_user.is_active = False
      new_user.set_password(user_form.cleaned_data['password']) # Zapisanie obiektu User.
      new_user.save()

      current_site = get_current_site(request)
      mail_subject = 'Activate your account.'
      message = render_to_string('account/acc_active_email.html', {
          'user': new_user,
          'domain': current_site.domain,
          'uid': urlsafe_base64_encode(force_bytes(new_user.pk)),
          'token': default_token_generator.make_token(new_user),
      })
      to_email = user_form.cleaned_data.get('email')
      email = EmailMessage(
          mail_subject, message, to=[to_email]
      )
      email.send()
      return render(request,
                'account/register_email.html',
                {'user_form': user_form}) 
  else:
    user_form = UserRegistrationForm()
  return render(request,
                'account/register.html',
                {'user_form': user_form})

def activate(request, uidb64, token):
    try:
        uid = urlsafe_base64_decode(uidb64).decode()
        user = UserModel._default_manager.get(pk=uid)
    except(TypeError, ValueError, OverflowError, User.DoesNotExist):
        user = None
    if user is not None and default_token_generator.check_token(user, token):
        user.is_active = True
        user.save()

        return render(request,
                'account/activate.html',
                {'section': 'activate', 'user': user})
    else:
        return HttpResponse('Activation link is invalid!')

@csrf_exempt
def active_subscription(request):
  if request.is_ajax():
    to_email = request.POST.get('user_email')

    with connections['users'].cursor() as cursor:
      cursor.execute("SELECT terra_user_email FROM terra_pre_registrated_users WHERE terra_user_email = %s", [to_email])
      row = cursor.fetchone()
    
    if row:
      return JsonResponse({'status': 'IN_DATABASE'})

    else:
      with connections['users'].cursor() as cursor:
        cursor.execute("INSERT INTO terra_pre_registrated_users(terra_user_email) VALUES (%s)", [to_email])

      email = EmailMessage(
        "Psst! You're on the list",
        render_to_string('account/subscription_active_email.html', {}),
        'hello@tergo.io',
        [to_email],
        ['hello@tergo.io'],
        reply_to = ['hello@tergo.io'],
      )
      email.content_subtype = 'html'

      try:
        email.send()

        return JsonResponse({'status': 'SUCCESS'})
      except:
        return JsonResponse({'status': 'FAILURE'})

  return HttpResponse('')

@csrf_exempt
def send_form(request):
  if request.is_ajax():
    from_email = request.POST.get('user_email')
    message_text = request.POST.get('message_text')
  
    email = EmailMessage(
      'Tergo Contact Form',
      message_text,
      from_email,
      ['hello@tergo.io'],
      reply_to = [from_email],
    )
    email.content_subtype = 'html'

    try:
      email.send()

      return JsonResponse({'status': 'SUCCESS'})
    except:
      return JsonResponse({'status': 'FAILURE'})

  return HttpResponse('')

@csrf_exempt
def send_results(request):
  if request.is_ajax():
    to_email = request.POST.get('user_email')
    lang = request.POST.get('lang')
    total_results = request.POST.get('total_results')
    tran_results = request.POST.get('tran_results')
    trav_results = request.POST.get('trav_results')
    l_results = request.POST.get('l_results')
    h_results = request.POST.get('h_results')

    if lang == 'eng':
      subject = 'Your carbon footprint: check your score on TerGo calculator! 🌍'

      body = render_to_string('account/results_email_eng.html', {
        'total_results': total_results,
        'tran_results': tran_results,
        'trav_results': trav_results,
        'l_results': l_results,
        'h_results': h_results,
      })
    else:
      subject = 'Twój ślad węglowy: sprawdź wynik obliczeń na kalkulatorze TerGo! 🌍'

      body = render_to_string('account/results_email.html', {
        'total_results': total_results,
        'tran_results': tran_results,
        'trav_results': trav_results,
        'l_results': l_results,
        'h_results': h_results,
      })

    email = EmailMessage(
      subject,
      body,
      'hello@tergo.io',
      [to_email],
      ['hello@tergo.io'],
    )
    email.content_subtype = 'html'

    try:
      email.send()

      return JsonResponse({'status': 'SUCCESS'})
    except:
      return JsonResponse({'status': 'FAILURE'})

  return HttpResponse('')
  
