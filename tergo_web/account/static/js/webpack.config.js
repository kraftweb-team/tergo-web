var path = require('path');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');

const rootPath = process.cwd();

module.exports = (env, options) => {
  const config = {
    paths: {
      root: rootPath,
      // assets: path.join(rootPath, 'assets'),
      dist: path.join(rootPath, 'dist'),
      debug: path.join(rootPath, 'debug'),
    },
    enabled: {
      sourceMaps: true,
      optimize: false,
      cacheBusting: false,
      watcher: true,
    },
  };

  return {
    entry: {
      main: [
        path.resolve(rootPath, 'app/index.js'),
        path.resolve(rootPath, 'scss/site.scss'),
      ],
      home: path.resolve(rootPath, 'app/home.js'),
      calculator: path.resolve(rootPath, 'app/calculator.js'),
      faq: path.resolve(rootPath, 'app/faq.js'),
      contact: path.resolve(rootPath, 'app/contact.js'),
      blog: path.resolve(rootPath, 'app/blog.js'),
      login: path.resolve(rootPath, 'app/login.js'),
      offset: path.resolve(rootPath, 'app/offset.js'),
    },
    // context: srcPath,
    output: {
      filename: '[name].bundle.js',
      path: options.mode === 'production' ? config.paths.dist : config.paths.debug,
      publicPath: '/',
    },
    /*
    optimization: {
      splitChunks: {
        chunks: 'all',
      },
    },
    */
    devtool: 'source-map',
    plugins: [new MiniCssExtractPlugin()],
    module: {
      rules: [{
          test: /\.(sa|sc|c)ss$/,
          include: config.paths.assets,
          use: [MiniCssExtractPlugin.loader, 'css-loader', 'sass-loader'],
        },
        {
          test: /\.(png|jpg|gif|svg)$/i,
          use: [{
            loader: 'url-loader',
            options: {
              limit: 8192,
              name: '[name].[hash:7].[ext]'
            },
          },],
        },
      ],
    },
  }
};
