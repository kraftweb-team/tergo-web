"""tergo_web URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.urls import path, re_path, include
from django.conf.urls import url
from django.contrib import admin
from django.views.generic import TemplateView

from django.conf import settings
from django.conf.urls.static import static

from account import views

urlpatterns = [
  path('admin/', admin.site.urls),
  path('account/', include('account.urls')),
  path('offset/', include('payment.urls')),

  path('active_subscription', views.active_subscription, name="active_subscription"),
  path('send_results', views.send_results, name="send_results"),
  path('send_form', views.send_form, name="send_form"),

  path('', TemplateView.as_view(template_name='index.html'), name='home'),
  
  re_path(r'index-en.html', TemplateView.as_view(template_name='index-en.html')),
  re_path(r'index.html', TemplateView.as_view(template_name='index.html')),
  re_path(r'carbon-balance.html', TemplateView.as_view(template_name='carbon-balance.html')),
  re_path(r'bilans.html', TemplateView.as_view(template_name='bilans.html')),
  re_path(r'kalkulator.html', TemplateView.as_view(template_name='kalkulator.html')),
  re_path(r'calculator.html', TemplateView.as_view(template_name='calculator.html')),
  re_path(r'biznes.html', TemplateView.as_view(template_name='biznes.html')),
  re_path(r'business.html', TemplateView.as_view(template_name='business.html')),
  re_path(r'ters-en.html', TemplateView.as_view(template_name='ters-en.html')),
  re_path(r'ters.html', TemplateView.as_view(template_name='ters.html')),
  re_path(r'carbon-neutral-printing.html', TemplateView.as_view(template_name='carbon-neutral-printing.html')),
  re_path(r'druk-carbon-neutral.html', TemplateView.as_view(template_name='druk-carbon-neutral.html')),
  re_path(r'kontakt.html', TemplateView.as_view(template_name='kontakt.html')),
  re_path(r'contact.html', TemplateView.as_view(template_name='contact.html')),
  re_path(r'faq-en.html', TemplateView.as_view(template_name='faq-en.html')),
  re_path(r'faq.html', TemplateView.as_view(template_name='faq.html')),
  re_path(r'regulamin.html', TemplateView.as_view(template_name='regulamin.html')),
  re_path(r'terms-and-conditions.html', TemplateView.as_view(template_name='terms-and-conditions.html')),
  re_path(r'polityka-prywatnosci-i-plikow-cookies.html', TemplateView.as_view(template_name='polityka-prywatnosci-i-plikow-cookies.html')),
  re_path(r'privacy-and-cookies-policy.html', TemplateView.as_view(template_name='privacy-and-cookies-policy.html')),
  re_path(r'blog-en.html', TemplateView.as_view(template_name='blog-en.html')),
  re_path(r'blog.html', TemplateView.as_view(template_name='blog.html')),
  re_path(r'czyscmy-chmury.html', TemplateView.as_view(template_name='czyscmy-chmury.html')),
  re_path(r'lets-clean-the-clouds.html', TemplateView.as_view(template_name='czyscmy-chmury.html')),
  re_path(r'ile-jablek-potrzeba-by-podgrzac-atmosfere.html', TemplateView.as_view(template_name='ile-jablek-potrzeba-by-podgrzac-atmosfere.html')),
  re_path(r'how-many-apples-does-it-take-to-warm-the-atmosphere.html', TemplateView.as_view(template_name='how-many-apples-does-it-take-to-warm-the-atmosphere.html')),
  re_path(r'kolej-na-ograniczenia-co2.html', TemplateView.as_view(template_name='kolej-na-ograniczenia-co2.html')),
  re_path(r'its-time-for-summer-and-reducing-CO2.html', TemplateView.as_view(template_name='its-time-for-summer-and-reducing-CO2.html')),
] + static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)


